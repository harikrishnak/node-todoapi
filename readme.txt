
Before hosting this app to heroku:

We need to create a cloud hosted mongodb instance.

2 Options to do 

1) Install mLab MongoDB plugin via heroku CLI 
  command : heroku plugins:install <plugin name>
   
2) or follow the below process

        1. login to mLab MongoDB site @ https://mlab.com
        2. create free account 
        3. create new MongoDB Deployments
        4. create db login user in the below grid in users tab on mlab site
        5. use this link format to connect the above created instance from app 
        
            mongodb://<dbuser>:<dbpassword>@ds129051.mlab.com:29051/todos

        6. change mongodb/mongoose connection string accordingly
        7. optionally you can set the above url on MONGODB_URI  as below
        
        heroku config:set MONGODB_URI=<yourUrlHere>

        and refer this as process.env.MONGODB_URI in code.
