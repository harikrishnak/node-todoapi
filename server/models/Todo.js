
var mongoose=require('mongoose');

var Todo = mongoose.model('Todo',{
    'text':{
        required:true,
        minlingth:1,
        type:String,
        trim:true
    },
    'completed':{
        default:false,
        type:Boolean
    },
    'completedAt':{
        default:null,
        type:Number
    }
});

module.exports={Todo};