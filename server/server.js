
const _= require('lodash');  
const express = require('express');
const bodyParser= require('body-parser');
const {ObjectID}=require('mongodb');


var PORT= process.env.PORT||3000;

var {mongoose}=  require('./db/mongoose');
var {Todo} = require('./models/Todo');
var {User} = require('./models/User');

var app= express();
app.use(bodyParser.json());

app.post('/todos',(req,resp)=>{
  
    new Todo({
        'text':req.body.text
    }).save().then((doc)=>{
        resp.status(200).send(doc);
    },(err)=>{
        console.log('Error:',err);
        resp.status(400).send(err);
    });
});

app.get('/todos',(req,resp)=>{
    Todo.find({})
    .then((doc)=>{
        resp.status(200).send({'todos':doc});},
    (err)=>{resp.status(400).send(err);})
    
});

app.get('/todos/:id',(req,resp)=>{
    var id= req.params.id;

    if(!ObjectID.isValid(id)){
        return resp.status(400).send({'message':'Invalid id.'});
    }
    
    Todo.find({'_id':id}).then((doc)=>{
            return resp.status(200).send({doc})
    },(err)=>{
        return resp.status(400).send();
    })
})
//remove all
app.delete('/todos/',(req,resp)=>{
    Todo.find({}).then(result=>{
        resp.status(200).send(result);
    },(err)=>{
        return resp.status(400).send(err);
    })
})

//remove by id
app.delete('/todos/:id',(req,resp)=>{
   var id=req.params.id;

   if(!ObjectID.isValid(id)){
    resp.status(404).send({'message':'Invalid id'});    
   }
    else{
        Todo.findByIdAndRemove({'_id':id}).remove().then(result=>{
            if(!result){
                return resp.status(404).send();                
            }
            resp.status(200).send(result);
        },(err)=>{
            resp.status(404).send(err);
        })
    }
})

//update todo item using patch. we included lodash library at this stage

app.patch('/todos/:id',(req,resp)=>{
    var id=req.params.id;

    //here, we are using underscore, to get only those properties which are allowd to update. so that
    //-  we can avoid accedental updates to other properties.
    var body=_.pick(req.body,['text','completed']);

    if(!ObjectID.isValid(id)){
        return resp.status(404).send({'message':'Invalid id.'});
    }
   
    //check safty condition. if completed is boolean or not
    if(_.isBoolean(body.completed) && body.completed){
        console.log(body);
        body.completedAt=new Date().getTime();
    }else{
        body.completed=false;
        body.completedAt=null;
    }

    Todo.findByIdAndUpdate({'_id':id},{$set:body},{new:true})
    .then(result=>{
        if(!result){
            return resp.status(404).send();
        }
        resp.status(200).send({result});
    }).catch((e)=>{
        resp.status(500).send(e);
    })
})

//users

  app.get('/users',(req,resp)=>{
      User.find({}).then((docs)=>{
          resp.status(200).send(docs);
      });
  })

  app.post('/users',(req,resp)=>{
      var body = _.pick(req.body,['email','password']);

      var user=new User(body);

      user.save().then(()=>{
           return user.generateAuthToken();
      }).then((token)=>{         
          resp.header('x-auth',token) .send(user);
      })
      .catch(e=>{
          resp.status(400).send(e);
      })
  })

app.listen(PORT,()=>{
    console.log(`Server started on ${PORT}`);
})


