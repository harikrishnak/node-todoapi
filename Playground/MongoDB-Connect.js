//const MongoClient=require('mongodb').MongoClient;

const {MongoClient,ObjectID} = require('mongodb');




MongoClient.connect('mongodb://localhost:27017/TodoApp',(err,client)=>{
        if(err){ return console.log('Unable to connect to mongodb server.', err); }

        console.log('Connected to mongodb server.');

        var db=client.db('TodoApp');

        db.collection('Todos').updateMany({'completed':false},{$set:{'completed':true}
        },{'returnOriginal':true}).then(resp=>{
            console.log(resp);

        })

        client.close();
})

